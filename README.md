# ruby-bundler

Test project with:

* **Language:** Ruby
* **Package Manager:** Bundler

## How to use

Please see the [usage documentation](https://gitlab.com/gitlab-org/security-products/tests/common#how-to-use) for Security Products test projects.

## Supported Security Products Features

| Feature             | Supported          |
|---------------------|--------------------|
| SAST                | :x:                |
| Dependency Scanning | :white_check_mark: |
| Container Scanning  | :warning: project is compatible but testing this is irrelevant |
| DAST                | :x:                |
| License Management  | :white_check_mark: |
